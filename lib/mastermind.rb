class Code
  attr_reader :pegs

  def initialize(pegs)
    @pegs = pegs
  end

  PEGS = {
    "B" => :blue,
    "G" => :green,
    "O" => :orange,
    "P" => :purple,
    "R" => :red,
    "Y" => :yellow
  }

  def self.parse(str)
    str.chars.each do |char|
      if PEGS[char.upcase].nil?
        raise "Error"
      end
    end
    Code.new(@pegs)
  end

  def self.random
    random_code = []
    4.times do
      random_code << PEGS.to_a.sample
    end
    Code.new(random_code.join)
  end

  def []
    @pegs.split("")
  end

  def exact_matches(code)
    matches = 0
    code.pegs.each do |peg|
      puts peg
    end
  end

end

class Game
  attr_reader :secret_code
  NEW_CODE = Code.new
  def initialize(code = Game.new(random))
    @secret_code = code
  end

end
