
class Code
  attr_reader :pegs

  PEGS = {
    "B" => :blue,
    "G" => :green,
    "O" => :orange,
    "P" => :purple,
    "R" => :red,
    "Y" => :yellow
  }

  def self.parse(str)
    chars = str.chars.map(&:upcase)
    raise ArgumentError unless chars - PEGS.keys == []
    pegs = chars.map { |char| PEGS[char] }
    self.new(pegs)
  end

  def self.random
    pegs = []
    4.times { pegs << PEGS.values.sample }
    self.new(pegs)
  end

  def initialize(pegs)
    @pegs = pegs
  end

  def [](idx)
    @pegs[idx]
  end

  def exact_matches(other_code)
    matches = 0
    other_code.pegs.each_with_index do |ele, ind|
      if other_code.pegs[ind] == @pegs[ind]
        matches += 1
      end
    end
    matches
  end

  def near_matches(other_code)
    near_matches = 0
    PEGS.values.each do |color|
      near_matches += [@pegs.count(color), other_code.pegs.count(color)].min
    end
    near_matches - exact_matches(other_code)
  end

  def ==(other)
    return false unless other.class == self.class
    @pegs == other.pegs
  end
end

class Game
  attr_reader :secret_code

  def initialize(secret_code = nil)
    @secret_code = secret_code || Code.random
  end

  def self.play
    guess = get_guess
    display_matches(guess)
    if @secret_code.exact_matches(guess) == 4
      puts "Congrats you guessed correctly"
    else
      self.play
    end
  end

  def get_guess
    puts "Input Guess"
    begin
      guess = gets.chomp.strip
      Code.parse(guess)
    rescue
      puts "Wrong color name, enter a valid combination:"
      retry
    end
  end

  def display_matches(code)
    puts "near matches : #{@secret_code.near_matches(code)} "
    puts "exact matches : #{@secret_code.exact_matches(code)} "
  end

end
